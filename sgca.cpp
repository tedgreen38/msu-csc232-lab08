/*This program conducts the following...
	-prompts the user to enter in numbers in a new array that has a max space of 50
	-reorganizes array to be in ascending order either via bubble or selection sort
		-this program will use the bubble sort
		
	-prompts user to enter a number they entered into the array so it can be found
	-uses binary sort to find the user's desired value.
*/

// Ted Green & Hannah Towle

#include <iostream>
using namespace std;

// function prototypes
void bubbleSortArray(int[], int);
void displayArray(int[], int);
int binarySearch(int[], int, int);	// function prototype 

const int SIZE = 50; 				//the maximum size of the array,

int main()
{
	
	int values[SIZE] = {};
	int value = 0;			//Value that user enters that will be put into the array.
	int size = 0;			//size of the array that user specifies
	int searched = 0;		//the number being searched in the binary search.
	int found = 0;			//the spot where the desired number was found.
	float mean = 0;			//calculates the mean of the array.
	float temp = 0;			//holds the calculated total of the array
	char ans;
	
	// Part 1: Creation of the array and it's values.
	cout << "Please input how big you want the array to be: ";
	cin >> size; 
	
	for (int i = 0; i < size; i++){
		cout << "Please input a number you would like to put into the array: ";
		cin >> value; 
		cout << endl;
		
		values[i] = value;	
	}

	// Part 2: Sort the array using a bubble sort.
	
	cout << "Entered Array: " << endl;
	displayArray(values, size); 
	
	bubbleSortArray(values, size);
	
	cout << "Sorted Array: " << endl;
	displayArray(values, size);

	// Part 3: Searching of elements using binary search.
	cout << "Enter an integer to search for:" << endl;
	cin >> searched;

	found = binarySearch(values, size, searched);
	// function call to perform the binary search
	// on array looking for an occurrence of value 

	if (found == -1)
		cout << "The value " << searched << " is not in the list" << endl;
	else
		cout << "The value " << searched << " is in position number "
		     << found + 1 << " of the list" << endl;
			 
	// Part 4: Calculate mean of array.
	for (int i = 0; i < size; i++){
		temp += values[i];
	}
	mean = temp / size;
	cout << "The mean for the entire array entered is: "<< mean << endl;
	return 0;
}

//******************************************************************
//	displayArray
//
//  task:	  to print the array
//  data in:  the array to be printed, the array size
//  data out: none
//
//******************************************************************

void displayArray(int array[], int elems)	// function heading
{		
	// displays the array 
	for (int count = 0; count < elems; count++)
		cout << array[count] << " ";
	cout << endl;

}

//******************************************************************
//	bubbleSortArray
//
//  task:	  to sort values of an array in ascending order (Modified to be in descending order now)
//  data in:  the array, the array size
//  data out: the sorted array
//
//******************************************************************

void bubbleSortArray(int array[], int elems)
{
	bool swap;
	int temp;
	int bottom = elems - 1;	// bottom indicates the end part of the
							// array where the largest values have
							// settled in order 
	do
	{
		swap = false;

		for (int count = 0; count < bottom; count++)
		{
			if (array[count] > array[count + 1])
			{		
				// the next three lines do a swap 
				temp = array[count];
				array[count] = array[count + 1];
				array[count + 1] = temp;

				swap = true; // indicates that a swap occurred
			}
		}

		bottom--;	// bottom is decremented by 1 since each pass through
					// the array adds one more value that is set in order

	} while (swap != false);
	// loop repeats until a pass through the array with
	// no swaps occurs
}
//*******************************************************************
//	binarySearch
//
//  task:	       This searches an array for a particular value
//  data in:	   List of values in an orderd array, the number of
//	               elements in the array, and the value searched for
//	               in the array
//  data returned: Position in the array of the value or -1 if value
//	               not found
//
//*******************************************************************

int binarySearch(int values[], int numElems, int searched)	// function heading
{
	int first = 0;				// First element of list
	int last = numElems - 1;	// last element of the list
	int middle;					// variable containing the current
								// middle value of the list

	while (first <= last)
	{
		middle = first + (last - first) / 2;

		if (values[middle] == searched)
			return middle;		// if value is in the middle, we are done

		else if (values[middle]>searched)
			last = middle - 1;	// toss out the second remaining half of

		else
			first = middle + 1;	// toss out the first remaining half of
								// the array and search the second
	}

	return -1;	// indicates that value is not in the array
}