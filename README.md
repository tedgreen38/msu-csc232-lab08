#Lab 8: Searching and Sorting Arrays

For this lab, please follow along with the instructions found in the PDF document (`LM_Chapter 08_8e.pdf`) contained in this repo (which is also attached in the Blackboard assignment). As usual,

* Work with one (or two) partners
* Fork this repo into your own **private** repo.
* Clone your private repo onto your development machine.
* Create a develop branch in which you will do your work.
* Upon completing the exercises in the lab, create a pull request to merge your develop branch into your master branch.
* Submit this assignment on Blackboard as specified at the end of this document.

## Prelab
If it hasn't already been printed for you, please print (logical) page 148 (physical page 12 of the file). Answer these pre-lab questions directly on the page. Only one page need be submitted for your team, but I suggest that every member of the team write up your answers and keep a "copy" just in case the one that gets handed in is lost. Hand in the one copy for your team to your lab instructor and make sure every team members name is *neatly* printed at the top of the page.

## Commits
At a minimum, please make commits after any exercise has you creating and/or modifying code. These commit messages should indicate which exercise the commit pertains too, e.g.,

```
$ git commit -m "CSC232-LAB08 - Completed Exercise 1 of Lab 8.1"
```

Notice that I've identified both the exercise number and lab number.

When it comes time to working on Lab 8.4 Student Generated Code Assignment, please do this work in the (empty) file named sgca.cpp.

**One final note**: Every source file included in this repo (with the exception of sgca.cpp which is empty) includes a `main()` method. Thus, when compiling these files, you must be specific as to which file you're compiling, i.e., you can't just type `g++ *.cpp`. For example, to compile `SampleProgram8-1.cpp` and run it, you must execute the following commands:

```
$ g++ SampleProgram8-1.cpp
$ ./a.exe
```

(This assumes execution in a Cygwin terminal window. If this were a Mac or some other Unix-based terminal, the executable would have been named `a.out`.)

## Due Date
Every individual must complete this lab by submitting the Lab 8 assignment on Blackboard by 23:59 Friday 28 October 2016. To get full credit, you must include a Text submission that

* Has a valid, working, hyperlink to your pull request for the work done in this lab.
* Identifies every member of your team.
* (Optional) Identify any issues you had in executing the requirements of this lab.
