// This program performs a linear search on a character array

// Ted Green & Hannah Towle

#include <iostream>
using namespace std;

int searchList(int[], int, int); // function prototype 

const int SIZE = 8;

int main()
{
	int word[SIZE] = {3,6,-19,5,5,0,2,99};
	int found;
	int num;
	bool redo = true;
	char ans;
	

	while(redo == true){
		cout << "Enter a number to search for:" << endl;
		cin >> num;

		found = searchList(word, SIZE, num);

		if (found == -1)
			cout << "The Number " << num
				<< " was not found in the list" << endl;
		else
			cout << "The Number " << num << " is in the " << found + 1
				<< " position of the list" << endl;
				
		cout << "Would you like to search for another number? (Y for yes/ N for no)" << endl;
		cin >> ans;
		
		if (ans == 'y' || ans == 'Y')
			redo = true;
		else if (ans == 'n'|| ans == 'N')
			redo = false;
		else
			redo = true;
	}


	return 0;
}

//*******************************************************************
//	searchList
//
//  task:	       This searches an array for a particular value
//  data in:	   List of values in an array, the number of
//	               elements in the array, and the value searched for
//	               in the array
//  data returned: Position in the array of the value or -1 if value
//	               not found
//
//*******************************************************************

int searchList(int List[], int numElems, int value)
{
	for (int count = 0; count <= numElems; count++)
	{
		if (List[count] == value)
			// each array entry is checked to see if it contains
			// the desired value. 
			return count;
			// if the desired value is found, the array subscript
			// count is returned to indicate the location in the array
	}

	return -1;	// if the value is not found, -1 is returned
}